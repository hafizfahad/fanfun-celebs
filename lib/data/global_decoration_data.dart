import 'package:flutter/material.dart';

OutlineInputBorder customTextFieldFocusedBorder = const OutlineInputBorder(
  borderRadius: BorderRadius.all(Radius.circular(10)),
  borderSide: BorderSide(color: Colors.white),
);

OutlineInputBorder customTextFieldBorder = const OutlineInputBorder(
  borderRadius: BorderRadius.all(Radius.circular(10)),
);
