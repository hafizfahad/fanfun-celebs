import 'package:fanfun_celebrity_app/models/video_request_model.dart';
import 'package:get_storage/get_storage.dart';

String smartLookSetupKey;
GetStorage box = GetStorage();

List<VideoRequestModel> getVideoRequestList = <VideoRequestModel>[];
List<VideoRequestModel> getPendingVideoRequestList = <VideoRequestModel>[];
List<VideoRequestModel> getCompletedVideoRequestList = <VideoRequestModel>[];
List<VideoRequestModel> getCanceledVideoRequestList = <VideoRequestModel>[];
List<VideoRequestModel> getExpiredVideoRequestList = <VideoRequestModel>[];
