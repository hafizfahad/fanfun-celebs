class VideoRequestModel {
  VideoRequestModel(
      {int id,
      int userId,
      String celebrityId,
      String toWhom,
      dynamic to,
      String from,
      String pronoun,
      String occasion,
      String instructions,
      String videoRequestStatus,
      int payment,
      String stripeInvoiceId,
      dynamic stripeInvoiceStatus,
      String stripePaymentIntentId,
      String createdAt,
      String updatedAt,
      dynamic expirationNotificationAt,
      String cancelationReason,
      User user,
      Video video}) {
    _id = id;
    _userId = userId;
    _celebrityId = celebrityId;
    _toWhom = toWhom;
    _to = to;
    _from = from;
    _pronoun = pronoun;
    _occasion = occasion;
    _instructions = instructions;
    _videoRequestStatus = videoRequestStatus;
    _payment = payment;
    _stripeInvoiceId = stripeInvoiceId;
    _stripeInvoiceStatus = stripeInvoiceStatus;
    _stripePaymentIntentId = stripePaymentIntentId;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _expirationNotificationAt = expirationNotificationAt;
    _cancelationReason = cancelationReason;
    _user = user;
    _video = video;
  }

  VideoRequestModel.fromJson(dynamic json) {
    _id = json['id'];
    _userId = json['user_id'];
    _celebrityId = json['celebrity_id'];
    _toWhom = json['to_whom'];
    _to = json['to'];
    _from = json['from'];
    _pronoun = json['pronoun'];
    _occasion = json['occasion'];
    _instructions = json['instructions'];
    _videoRequestStatus = json['video_request_status'];
    _payment = json['payment'];
    _stripeInvoiceId = json['stripe_invoice_id'];
    _stripeInvoiceStatus = json['stripe_invoice_status'];
    _stripePaymentIntentId = json['stripe_payment_intent_id'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _expirationNotificationAt = json['expiration_notification_at'];
    _cancelationReason = json['cancelation_reason'];
    _user = json['user'] != null ? User.fromJson(json['user']) : null;
    _video = json['video'] != null ? Video.fromJson(json['video']) : null;
  }

  int _id;
  int _userId;
  String _celebrityId;
  String _toWhom;
  dynamic _to;
  String _from;
  String _pronoun;
  String _occasion;
  String _instructions;
  String _videoRequestStatus;
  int _payment;
  String _stripeInvoiceId;
  dynamic _stripeInvoiceStatus;
  String _stripePaymentIntentId;
  String _createdAt;
  String _updatedAt;
  dynamic _expirationNotificationAt;
  String _cancelationReason;
  User _user;
  Video _video;

  int get id => _id;
  int get userId => _userId;
  String get celebrityId => _celebrityId;
  String get toWhom => _toWhom;
  dynamic get to => _to;
  String get from => _from;
  String get pronoun => _pronoun;
  String get occasion => _occasion;
  String get instructions => _instructions;
  String get videoRequestStatus => _videoRequestStatus;
  int get payment => _payment;
  String get stripeInvoiceId => _stripeInvoiceId;
  dynamic get stripeInvoiceStatus => _stripeInvoiceStatus;
  String get stripePaymentIntentId => _stripePaymentIntentId;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  dynamic get expirationNotificationAt => _expirationNotificationAt;
  String get cancelationReason => _cancelationReason;
  User get user => _user;
  Video get video => _video;

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = <String, dynamic>{};
    map['id'] = _id;
    map['user_id'] = _userId;
    map['celebrity_id'] = _celebrityId;
    map['to_whom'] = _toWhom;
    map['to'] = _to;
    map['from'] = _from;
    map['pronoun'] = _pronoun;
    map['occasion'] = _occasion;
    map['instructions'] = _instructions;
    map['video_request_status'] = _videoRequestStatus;
    map['payment'] = _payment;
    map['stripe_invoice_id'] = _stripeInvoiceId;
    map['stripe_invoice_status'] = _stripeInvoiceStatus;
    map['stripe_payment_intent_id'] = _stripePaymentIntentId;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['expiration_notification_at'] = _expirationNotificationAt;
    map['cancelation_reason'] = _cancelationReason;
    if (_user != null) {
      map['user'] = _user.toJson();
    }
    if (_video != null) {
      map['video'] = _video.toJson();
    }
    return map;
  }
}

class Video {
  Video(
      {String id,
      int videoRequestId,
      String videoUrl,
      int isPublic,
      String reviewRate,
      String reviewObservation,
      String createdAt,
      String updatedAt,
      int released}) {
    _id = id;
    _videoRequestId = videoRequestId;
    _videoUrl = videoUrl;
    _isPublic = isPublic;
    _reviewRate = reviewRate;
    _reviewObservation = reviewObservation;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _released = released;
  }

  Video.fromJson(dynamic json) {
    _id = json['id'];
    _videoRequestId = json['video_request_id'];
    _videoUrl = json['video_url'];
    _isPublic = json['is_public'];
    _reviewRate = json['review_rate'];
    _reviewObservation = json['review_observation'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _released = json['released'];
  }

  String _id;
  int _videoRequestId;
  String _videoUrl;
  int _isPublic;
  String _reviewRate;
  String _reviewObservation;
  String _createdAt;
  String _updatedAt;
  int _released;

  String get id => _id;
  int get videoRequestId => _videoRequestId;
  String get videoUrl => _videoUrl;
  int get isPublic => _isPublic;
  String get reviewRate => _reviewRate;
  String get reviewObservation => _reviewObservation;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  int get released => _released;

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = <String, dynamic>{};
    map['id'] = _id;
    map['video_request_id'] = _videoRequestId;
    map['video_url'] = _videoUrl;
    map['is_public'] = _isPublic;
    map['review_rate'] = _reviewRate;
    map['review_observation'] = _reviewObservation;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['released'] = _released;
    return map;
  }
}

class User {
  User(
      {int id,
      String name,
      String email,
      String emailVerifiedAt,
      dynamic currentTeamId,
      dynamic currentConnectedAccountId,
      dynamic profilePhotoPath,
      String createdAt,
      String updatedAt,
      String stripeId,
      String cardBrand,
      String cardLastFour,
      dynamic trialEndsAt,
      dynamic celebrityId,
      dynamic phone,
      String profilePhotoUrl,
      dynamic celebrity}) {
    _id = id;
    _name = name;
    _email = email;
    _emailVerifiedAt = emailVerifiedAt;
    _currentTeamId = currentTeamId;
    _currentConnectedAccountId = currentConnectedAccountId;
    _profilePhotoPath = profilePhotoPath;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _stripeId = stripeId;
    _cardBrand = cardBrand;
    _cardLastFour = cardLastFour;
    _trialEndsAt = trialEndsAt;
    _celebrityId = celebrityId;
    _phone = phone;
    _profilePhotoUrl = profilePhotoUrl;
    _celebrity = celebrity;
  }

  User.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _email = json['email'];
    _emailVerifiedAt = json['email_verified_at'];
    _currentTeamId = json['current_team_id'];
    _currentConnectedAccountId = json['current_connected_account_id'];
    _profilePhotoPath = json['profile_photo_path'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _stripeId = json['stripe_id'];
    _cardBrand = json['card_brand'];
    _cardLastFour = json['card_last_four'];
    _trialEndsAt = json['trial_ends_at'];
    _celebrityId = json['celebrity_id'];
    _phone = json['phone'];
    _profilePhotoUrl = json['profile_photo_url'];
    _celebrity = json['celebrity'];
  }

  int _id;
  String _name;
  String _email;
  String _emailVerifiedAt;
  dynamic _currentTeamId;
  dynamic _currentConnectedAccountId;
  dynamic _profilePhotoPath;
  String _createdAt;
  String _updatedAt;
  String _stripeId;
  String _cardBrand;
  String _cardLastFour;
  dynamic _trialEndsAt;
  dynamic _celebrityId;
  dynamic _phone;
  String _profilePhotoUrl;
  dynamic _celebrity;

  int get id => _id;
  String get name => _name;
  String get email => _email;
  String get emailVerifiedAt => _emailVerifiedAt;
  dynamic get currentTeamId => _currentTeamId;
  dynamic get currentConnectedAccountId => _currentConnectedAccountId;
  dynamic get profilePhotoPath => _profilePhotoPath;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  String get stripeId => _stripeId;
  String get cardBrand => _cardBrand;
  String get cardLastFour => _cardLastFour;
  dynamic get trialEndsAt => _trialEndsAt;
  dynamic get celebrityId => _celebrityId;
  dynamic get phone => _phone;
  String get profilePhotoUrl => _profilePhotoUrl;
  dynamic get celebrity => _celebrity;

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['email'] = _email;
    map['email_verified_at'] = _emailVerifiedAt;
    map['current_team_id'] = _currentTeamId;
    map['current_connected_account_id'] = _currentConnectedAccountId;
    map['profile_photo_path'] = _profilePhotoPath;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['stripe_id'] = _stripeId;
    map['card_brand'] = _cardBrand;
    map['card_last_four'] = _cardLastFour;
    map['trial_ends_at'] = _trialEndsAt;
    map['celebrity_id'] = _celebrityId;
    map['phone'] = _phone;
    map['profile_photo_url'] = _profilePhotoUrl;
    map['celebrity'] = _celebrity;
    return map;
  }
}
