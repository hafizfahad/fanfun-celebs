import 'package:flutter/material.dart';

class ColorButton extends StatelessWidget {
  const ColorButton({
    @required this.text,
  });
  final String text;

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 4,
      borderRadius: BorderRadius.circular(10.0),
      child: Container(
        width: MediaQuery.of(context).size.width * .85,
        height: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: const LinearGradient(colors: <Color>[
            Color(0xFFFEE140),
            Color(0xFFFA709A),
          ]),
        ),
        child: Center(
          child: Text(
            text,
            style: Theme.of(context).textTheme.headline4,
          ),
        ),
      ),
    );
  }
}
