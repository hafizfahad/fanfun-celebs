import 'package:fanfun_celebrity_app/components/gradient_text.dart';
import 'package:fanfun_celebrity_app/utils/colors.dart';
import 'package:flutter/material.dart';

class CustomGradientButton extends StatelessWidget {
  const CustomGradientButton({this.text, this.textFontSize, this.buttonHeight});
  final String text;
  final double textFontSize;
  final double buttonHeight;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: buttonHeight,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: const LinearGradient(
          colors: <Color>[
            customGradientFirstColor,
            customGradientSecondColor,
          ],
          end: Alignment.bottomRight,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(1.5),
        child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Theme.of(context).scaffoldBackgroundColor),
            child: Center(
              child: GradientText(
                text,
                fontSize: textFontSize,
                gradient: const LinearGradient(
                  colors: <Color>[
                    customGradientFirstColor,
                    customGradientSecondColor,
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
            )),
      ),
    );
  }
}
