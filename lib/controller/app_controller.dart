import 'package:fanfun_celebrity_app/models/video_request_model.dart';
import 'package:get/get.dart';

class AppController extends GetxController {
  bool getVideoRequestCheck = false;
  void changeGetVideoRequestCheck({bool updatedValue}) {
    getVideoRequestCheck = updatedValue;
    update();
  }

  bool loaderProgressCheck = false;
  void updateLoaderProgressCheck({bool updatedValue}) {
    loaderProgressCheck = updatedValue;
    update();
  }

  VideoRequestModel getSelectedVideoRequestModel;
  void updateGetSelectedVideoRequestModel({VideoRequestModel updatedValue}) {
    getSelectedVideoRequestModel = updatedValue;
    update();
  }
}
