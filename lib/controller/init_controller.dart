import 'package:fanfun_celebrity_app/data/global_data.dart';
import 'package:fanfun_celebrity_app/screens/bottom_navbar_screen.dart';
import 'package:fanfun_celebrity_app/screens/login_screen.dart';
import 'package:flutter/material.dart';

class ScreenController extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    if (box.hasData('isLogin')) {
      return BottomNavBAr();
    } else {
      return LoginScreen();
    }
  }
}
