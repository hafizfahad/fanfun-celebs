// Copyright 2013 The Flutter Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// ignore_for_file: public_member_api_docs
import 'dart:developer';
import 'package:camera/camera.dart';
import 'package:fanfun_celebrity_app/controller/app_controller.dart';
import 'package:fanfun_celebrity_app/data/global_data.dart';
import 'package:fanfun_celebrity_app/screens/splash_screen.dart';
import 'package:fanfun_celebrity_app/screens/telepromter_screen.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:get_storage/get_storage.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smartlook/smartlook.dart';

Future<void> main() async {
  // Fetch the available cameras before initializing the app.

  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();
  await GetStorage.init();
  final FirebaseAnalytics analytics = FirebaseAnalytics();

  // Pass all uncaught errors from the framework to Crashlytics.
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;

  //-----load-configurations-from-local-json
  await GlobalConfiguration().loadFromAsset('configurations');
  smartLookSetupKey = GlobalConfiguration().get('smartLookSetupKey').toString();

  ///-------------------------------------------
  final SetupOptions options = (SetupOptionsBuilder(smartLookSetupKey)
        ..Fps = 2
        ..StartNewSession = true)
      .build();
  Smartlook.setupAndStartRecording(options);
  // calling all functions to make sure nothing crashes
  Smartlook.setEventTrackingMode(EventTrackingMode.FULL_TRACKING);
  final List<EventTrackingMode> eventTrackingModeList =
      List<EventTrackingMode>.filled(2, EventTrackingMode.NO_TRACKING,
          growable: true);
  eventTrackingModeList[0] = EventTrackingMode.FULL_TRACKING;
  eventTrackingModeList[1] = EventTrackingMode.IGNORE_USER_INTERACTION;
  Smartlook.setEventTrackingModes(eventTrackingModeList);
  Smartlook.registerIntegrationListener(CustomIntegrationListener());
  Smartlook.setUserIdentifier(
      'FanFun User', <String, dynamic>{'flutter-usr-prop': 'valueX'});
  Smartlook.setGlobalEventProperty('flutter_global', 'value_', true);
  Smartlook.enableWebviewRecording(true);
  Smartlook.enableCrashlytics(true);
  Smartlook.setReferrer('referer', 'source');
  Smartlook.getDashboardSessionUrl(true);

  ///-------------------------------------------

  try {
    cameras = <CameraDescription>[];
    cameras = await availableCameras();
  } on CameraException catch (e) {
    logError(e.code, e.description);
  }

  SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: Color(0xff333B40)));
  SystemChrome.setPreferredOrientations(<DeviceOrientation>[
    DeviceOrientation.portraitUp,
  ]);

  Get.put(AppController());
  runApp(GetMaterialApp(
    debugShowCheckedModeBanner: false,
    navigatorObservers: <NavigatorObserver>[
      FirebaseAnalyticsObserver(analytics: analytics),
    ],
    theme: ThemeData(
        scaffoldBackgroundColor: const Color(0xff202328),
        primaryColor: const Color(0xff202328),
        textTheme: TextTheme(
          headline1: GoogleFonts.inter(
            color: Colors.white,
            fontSize: 18,
          ),
          headline4: GoogleFonts.inter(
            color: Colors.white,
            fontSize: 18,
            fontWeight: FontWeight.w300,
          ),
          headline2: GoogleFonts.inter(
            color: Colors.white,
            fontSize: 40.0,
            fontWeight: FontWeight.w200,
            letterSpacing: 1.5,
          ),
          headline3: GoogleFonts.inter(
            color: Colors.white,
            fontSize: 16,
          ),
        )),
    home: SplashScreen(),
  ));
}

class CustomIntegrationListener implements IntegrationListener {
  @override
  void onSessionReady(String dashboardSessionUrl) {
    log('DashboardUrl:');
    log(dashboardSessionUrl);
  }

  @override
  void onVisitorReady(String dashboardVisitorUrl) {
    log('DashboardVisitorUrl:');
    log(dashboardVisitorUrl);
  }
}
