import 'package:dio/dio.dart';

void setHeader(Dio dio, String key, String value) {
  dio.options.headers[key] = value;
}

void setHeaderAccept(Dio dio) {
  dio.options.headers['Accept'] = 'application/json';
}

void setHeaderContent(Dio dio) {
  dio.options.headers['Content-Type'] = 'application/json';
}
