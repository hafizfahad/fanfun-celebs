import 'dart:developer';
import 'package:dio/dio.dart' as dio_instance;
import 'package:fanfun_celebrity_app/controller/app_controller.dart';
import 'package:fanfun_celebrity_app/screens/bottom_navbar_screen.dart';
import 'package:fanfun_celebrity_app/services/header_services.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

Future<void> putMethod(String url, Map<String, dynamic> data, String apiCall,
    BuildContext context, Function validator) async {
  final GetStorage box = GetStorage();
  dio_instance.Response<dynamic> response;
  final dio_instance.Dio dio = dio_instance.Dio();

  setHeaderAccept(dio);
  setHeaderContent(dio);

  if (apiCall != 'login' && apiCall != 'register') {
    setHeader(dio, 'Authorization', 'Bearer ${box.read('accessToken')}');
  }
  try {
    response = await dio.put(url, data: data);
    Get.find<AppController>().updateLoaderProgressCheck(updatedValue: false);

    if (apiCall == 'sendRequest') {
      log('sendRequestStatusCode---->> ${response.statusCode}');
      log('sendRequestResponse---->> ${response.data}');
      // Get.find<AppController>().updateLoaderProgressCheck(updatedValue: false);
      // Get.offAll(BottomNavBAr());
    } else if (apiCall == 'cancelRequest') {
      log('cancelRequestStatusCode---->> ${response.statusCode}');
      log('cancelRequestResponse---->> ${response.data}');
      Get.find<AppController>().updateLoaderProgressCheck(updatedValue: false);
      Get.offAll(BottomNavBAr());
    }
  } on dio_instance.DioError catch (e) {
    Get.find<AppController>().updateLoaderProgressCheck(updatedValue: false);
    Get.snackbar('Error', '${e.response.data['message']}',
        backgroundColor: Colors.white70);
    log('Post Error ------->> ${e.response}');
  }
}
