import 'dart:developer';
import 'package:dio/dio.dart' as dio_instance;
import 'package:fanfun_celebrity_app/controller/app_controller.dart';
import 'package:fanfun_celebrity_app/data/global_data.dart';
import 'package:fanfun_celebrity_app/models/video_request_model.dart';
import 'package:fanfun_celebrity_app/services/header_services.dart';
import 'package:fanfun_celebrity_app/screens/orders_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

dynamic getMethod(String url, BuildContext context, String apiCall,
    Map<String, dynamic> extraData) async {
  dio_instance.Response<dynamic> response;
  final dio_instance.Dio dio = dio_instance.Dio();

  setHeader(dio, 'Authorization', 'Bearer ${box.read('accessToken')}');

  try {
    response = await dio.get(url, queryParameters: extraData);
    switch (apiCall) {
      case 'getVideoRequests':
        return <void>{
          log('getVideoRequests statusCode----->> ${response.statusCode}'),
          log('getVideoRequests response----->> ${response.data}'),
          getVideoRequestList = <VideoRequestModel>[],
          getPendingVideoRequestList = <VideoRequestModel>[],
          getCompletedVideoRequestList = <VideoRequestModel>[],
          getCanceledVideoRequestList = <VideoRequestModel>[],
          response.data['data'].forEach((dynamic videoRequest) {
            getVideoRequestList.add(VideoRequestModel.fromJson(videoRequest));
            if (videoRequest['video_request_status'].toString() == 'waiting') {
              getPendingVideoRequestList
                  .add(VideoRequestModel.fromJson(videoRequest));
            } else if (videoRequest['video_request_status'].toString() ==
                'completed') {
              getCompletedVideoRequestList
                  .add(VideoRequestModel.fromJson(videoRequest));
            } else if (videoRequest['video_request_status'].toString() ==
                'canceled') {
              getCanceledVideoRequestList
                  .add(VideoRequestModel.fromJson(videoRequest));
            } else if (videoRequest['video_request_status'].toString() ==
                'expired') {
              getExpiredVideoRequestList
                  .add(VideoRequestModel.fromJson(videoRequest));
            }
          }),
          Get.find<AppController>()
              .changeGetVideoRequestCheck(updatedValue: true),
        };
        break;
      case 'cancelRequest':
        return <void>{
          log('cancelRequest statusCode----->> ${response.statusCode}'),
          log('cancelRequest response----->> ${response.data}'),
          Get.find<AppController>()
              .updateLoaderProgressCheck(updatedValue: false),
          Get.offAll(OrdersScreen()),
        };
        break;
      default:
    }
  } on dio_instance.DioError catch (e) {
    Get.find<AppController>().changeGetVideoRequestCheck(updatedValue: true);
    log('check me get Method Here');
    log('Get Error -------->> ${e.message}');
  }
  // if rest api success data reutrn

  // if success false then show toast
}
