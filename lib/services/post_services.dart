import 'dart:developer';

import 'package:dio/dio.dart' as dio_instance;
import 'package:fanfun_celebrity_app/controller/app_controller.dart';
import 'package:fanfun_celebrity_app/screens/bottom_navbar_screen.dart';
import 'package:fanfun_celebrity_app/services/header_services.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

Future<void> postMethod(String url, Map<String, dynamic> data, String apiCall,
    BuildContext context, Function validator) async {
  final GetStorage box = GetStorage();
  dio_instance.Response<dynamic> response;
  final dio_instance.Dio dio = dio_instance.Dio();

  setHeaderAccept(dio);
  setHeaderContent(dio);

  if (apiCall != 'login' && apiCall != 'register') {
    setHeader(dio, 'Authorization', 'Bearer ${box.read('accessToken')}');
  }
  try {
    response = await dio.post(url, data: data);
    Get.find<AppController>().updateLoaderProgressCheck(updatedValue: false);

    if (apiCall == 'login') {
      log('loginStatusCode---->> ${response.statusCode}');
      log('loginResponse---->> ${response.data}');
      box.write('accessToken', response.data);
      box.write('isLogin', 'true');
      log("Token--->> ${box.read("accessToken")}");
      Get.offAll(BottomNavBAr());
    } else if (apiCall == 'fcmToken') {
      box.write('fcmTokenSaved', 'true');
      log('fcmTokenStatusCode---->> ${response.statusCode}');
      log('fcmTokenResponse---->> ${response.data}');
    }
  } on dio_instance.DioError catch (e) {
    Get.find<AppController>().updateLoaderProgressCheck(updatedValue: false);
    Get.snackbar('Error', '${e.response.data['message']}',
        backgroundColor: Colors.white70);
    log('Post Error ------->> ${e.response}');
  }
}
