import 'package:flutter/material.dart';

const Color customBottomBarColor = Color(0xff333B40);
const Color customGradientFirstColor = Color(0xffEB0D79);
const Color customGradientSecondColor = Color(0xffFEA134);
const Color customLightGreyColor = Color(0xff5E6671);
const Color customPrimaryGreyColor = Color(0xff333B40);
const Color customCategoryYellowColor = Color(0xffF2C94C);
const Color customCategoryPurpleColor = Color(0xffB548E9);
const Color customCategoryRedColor = Color(0xffE95147);
const Color customCategoryGreenColor = Color(0xff47E980);
const Color customCategoryBlueColor = Color(0xff69D8E7);
const Color customOrangeColor = Color(0xffF66052);
const Color customOrangeTextColor = Color(0xffE78769);
const Color customWhiteColor = Color(0xffFFFFFD);
