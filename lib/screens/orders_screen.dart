import 'package:fanfun_celebrity_app/controller/app_controller.dart';
import 'package:fanfun_celebrity_app/data/global_data.dart';
import 'package:fanfun_celebrity_app/screens/order_detail_screen.dart';
import 'package:fanfun_celebrity_app/services/api_urls.dart';
import 'package:fanfun_celebrity_app/services/get_services.dart';
import 'package:fanfun_celebrity_app/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:skeleton_loader/skeleton_loader.dart';
import 'package:fanfun_celebrity_app/services/post_services.dart';
import 'dart:io' show Platform;

class OrdersScreen extends StatefulWidget {
  @override
  _OrdersScreenState createState() => _OrdersScreenState();
}

class _OrdersScreenState extends State<OrdersScreen>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 4, vsync: this);
    getMethod(getVideoRequest, context, 'getVideoRequests', null);

    if (!box.hasData('fcmTokenSaved')) {
      postMethod(
          fcmTokenApi,
          <String, dynamic>{
            'token': box.read('fcm_token'),
            'platform': Platform.operatingSystem.toString(),
          },
          'fcmToken',
          context,
          null);
    }
  }

  Future<Null> _refreshRequests(BuildContext context) {
    getMethod(getVideoRequest, context, 'getVideoRequests', null);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 0,
        centerTitle: false,
        title: Text(
          /*My Orders*/ 'Meus Pedidos',
          style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 24),
        ),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
              colors: <Color>[Color(0xFF1E2026), Color(0xFF23252E)],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 40,
                width: MediaQuery.of(context).size.width * 0.9,
                child: FittedBox(
                  child: SizedBox(
                    height: 40,
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: TabBar(
                      isScrollable: true,
                      controller: tabController,
                      labelColor: Colors.white,
                      unselectedLabelColor: Colors.white,
                      labelStyle:
                          Theme.of(context).textTheme.headline3.copyWith(
                                fontWeight: FontWeight.w400,
                              ),
                      indicator: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: <Color>[
                            customGradientFirstColor,
                            customGradientSecondColor,
                          ],
                          end: Alignment.bottomRight,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      tabs: <Widget>[
                        Center(
                          child: Text(
                            'Aguardando', //waiting
                            style:
                                Theme.of(context).textTheme.headline3.copyWith(
                                      fontWeight: FontWeight.w400,
                                    ),
                          ),
                        ),
                        Center(
                          child: Text(
                            'Finalizados', //completed
                            style:
                                Theme.of(context).textTheme.headline3.copyWith(
                                      fontWeight: FontWeight.w400,
                                    ),
                          ),
                        ),
                        Center(
                          child: Text(
                            'Cancelados', //canceled
                            style:
                                Theme.of(context).textTheme.headline3.copyWith(
                                      fontWeight: FontWeight.w400,
                                    ),
                          ),
                        ),
                        Center(
                          child: Text(
                            'Expirados', //expired
                            style:
                                Theme.of(context).textTheme.headline3.copyWith(
                                      fontWeight: FontWeight.w400,
                                    ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              GetBuilder<AppController>(
                builder: (AppController appController) => Expanded(
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height * .73,
                    child: TabBarView(
                      controller: tabController,
                      children: <Widget>[
                        ///waiting section
                        !appController.getVideoRequestCheck
                            ? ListView(
                                children: List<Widget>.generate(5, (int index) {
                                  return SkeletonLoader(
                                    highlightColor: Colors.grey,
                                    builder: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        height: 80,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                .95,
                                        decoration: BoxDecoration(
                                            color: customPrimaryGreyColor,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: Padding(
                                          padding: const EdgeInsets.all(10.0),
                                        ),
                                      ),
                                    ),
                                  );
                                }),
                              )
                            : getPendingVideoRequestList.isEmpty
                                ? Center(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          'Nenhum pedido aguardando!', //Your Waiting orders are empty
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline1
                                              .copyWith(
                                                  fontWeight: FontWeight.w700),
                                        ),
                                      ],
                                    ),
                                  )
                                : RefreshIndicator(
                                    child: ListView(
                                      children: List<Widget>.generate(
                                          getPendingVideoRequestList.length,
                                          (int index) {
                                        return InkWell(
                                          onTap: () {
                                            Get.find<AppController>()
                                                .updateGetSelectedVideoRequestModel(
                                                    updatedValue:
                                                        getPendingVideoRequestList[
                                                            index]);
                                            Get.to(OrderDetailScreen());
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .95,
                                              decoration: BoxDecoration(
                                                  color: customPrimaryGreyColor,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(10.0),
                                                child: Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    CircleAvatar(
                                                      backgroundColor:
                                                          Colors.white,
                                                      radius: 25,
                                                      child: ClipRRect(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(40),
                                                          child: getPendingVideoRequestList[
                                                                          index]
                                                                      .user
                                                                      .profilePhotoUrl !=
                                                                  null
                                                              ? Image.network(
                                                                  '${getPendingVideoRequestList[index].user.profilePhotoUrl}')
                                                              : Icon(
                                                                  Icons.person,
                                                                  size: 35,
                                                                  color: Colors
                                                                      .grey,
                                                                )),
                                                    ),
                                                    Expanded(
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .symmetric(
                                                                horizontal: 10),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: <Widget>[
                                                            Text(
                                                              '${getPendingVideoRequestList[index].createdAt.split('T').first}',
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .headline4
                                                                  .copyWith(
                                                                    fontSize:
                                                                        12,
                                                                  ),
                                                              maxLines: 2,
                                                            ),
                                                            const SizedBox(
                                                              height: 8,
                                                            ),
                                                            Text(
                                                              '${getPendingVideoRequestList[index].user.name}',
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .headline3
                                                                  .copyWith(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                  ),
                                                              softWrap: true,
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height: 30,
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        gradient:
                                                            LinearGradient(
                                                          colors: <Color>[
                                                            customGradientFirstColor,
                                                            customGradientSecondColor,
                                                          ],
                                                          end: Alignment
                                                              .bottomRight,
                                                        ),
                                                      ),
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .fromLTRB(
                                                                8, 0, 8, 0),
                                                        child: Center(
                                                            child: Text(
                                                          'Aguardando', //waiting
                                                          style: Theme.of(
                                                                  context)
                                                              .textTheme
                                                              .headline3
                                                              .copyWith(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize: 12),
                                                        )),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        );
                                      }),
                                    ),
                                    onRefresh: () async {
                                      getMethod(getVideoRequest, context,
                                          'getVideoRequests', null);
                                    }),

                        ///completed section
                        !appController.getVideoRequestCheck
                            ? ListView(
                                children: List<Widget>.generate(5, (int index) {
                                  return SkeletonLoader(
                                    highlightColor: Colors.grey,
                                    builder: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        height: 80,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                .95,
                                        decoration: BoxDecoration(
                                            color: customPrimaryGreyColor,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: Padding(
                                          padding: const EdgeInsets.all(10.0),
                                        ),
                                      ),
                                    ),
                                  );
                                }),
                              )
                            : getCompletedVideoRequestList.isEmpty
                                ? Center(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          'Nenhum pedido finalizados!', //Your completed orders are empty
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline1
                                              .copyWith(
                                                  fontWeight: FontWeight.w700),
                                        ),
                                      ],
                                    ),
                                  )
                                : RefreshIndicator(
                                    child: ListView(
                                      children: List<Widget>.generate(
                                          getCompletedVideoRequestList.length,
                                          (int index) {
                                        return InkWell(
                                          onTap: () {
                                            Get.find<AppController>()
                                                .updateGetSelectedVideoRequestModel(
                                                    updatedValue:
                                                        getCompletedVideoRequestList[
                                                            index]);
                                            Get.to(OrderDetailScreen());
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .95,
                                              decoration: BoxDecoration(
                                                  color: customPrimaryGreyColor,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(10.0),
                                                child: Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    CircleAvatar(
                                                      backgroundColor:
                                                          Colors.white,
                                                      radius: 25,
                                                      child: ClipRRect(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(40),
                                                          child: getCompletedVideoRequestList[
                                                                          index]
                                                                      .user
                                                                      .profilePhotoUrl !=
                                                                  null
                                                              ? Image.network(
                                                                  '${getCompletedVideoRequestList[index].user.profilePhotoUrl}')
                                                              : Icon(
                                                                  Icons.person,
                                                                  size: 35,
                                                                  color: Colors
                                                                      .grey,
                                                                )),
                                                    ),
                                                    Expanded(
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .symmetric(
                                                                horizontal: 10),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: <Widget>[
                                                            Text(
                                                              '${getCompletedVideoRequestList[index].createdAt.split('T').first}',
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .headline4
                                                                  .copyWith(
                                                                    fontSize:
                                                                        12,
                                                                  ),
                                                              maxLines: 2,
                                                            ),
                                                            const SizedBox(
                                                              height: 8,
                                                            ),
                                                            Text(
                                                              '${getCompletedVideoRequestList[index].user.name}',
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .headline3
                                                                  .copyWith(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                  ),
                                                              softWrap: true,
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height: 30,
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        gradient:
                                                            LinearGradient(
                                                          colors: <Color>[
                                                            customGradientFirstColor,
                                                            customGradientSecondColor,
                                                          ],
                                                          end: Alignment
                                                              .bottomRight,
                                                        ),
                                                      ),
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .fromLTRB(
                                                                8, 0, 8, 0),
                                                        child: Center(
                                                            child: Text(
                                                          'Finalizados', //completed
                                                          style: Theme.of(
                                                                  context)
                                                              .textTheme
                                                              .headline3
                                                              .copyWith(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize: 12),
                                                        )),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        );
                                      }),
                                    ),
                                    onRefresh: () async {
                                      getMethod(getVideoRequest, context,
                                          'getVideoRequests', null);
                                    }),

                        ///cancel section
                        !appController.getVideoRequestCheck
                            ? ListView(
                                children: List<Widget>.generate(5, (int index) {
                                  return SkeletonLoader(
                                    highlightColor: Colors.grey,
                                    builder: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        height: 80,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                .95,
                                        decoration: BoxDecoration(
                                            color: customPrimaryGreyColor,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: Padding(
                                          padding: const EdgeInsets.all(10.0),
                                        ),
                                      ),
                                    ),
                                  );
                                }),
                              )
                            : getCanceledVideoRequestList.isEmpty
                                ? Center(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          'Nenhum pedido cancelados!', //Your canceled orders are empty
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline1
                                              .copyWith(
                                                  fontWeight: FontWeight.w700),
                                        ),
                                      ],
                                    ),
                                  )
                                : RefreshIndicator(
                                    child: ListView(
                                      children: List<Widget>.generate(
                                          getCanceledVideoRequestList.length,
                                          (int index) {
                                        return InkWell(
                                          onTap: () {
                                            Get.find<AppController>()
                                                .updateGetSelectedVideoRequestModel(
                                                    updatedValue:
                                                        getCanceledVideoRequestList[
                                                            index]);
                                            Get.to(OrderDetailScreen());
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .95,
                                              decoration: BoxDecoration(
                                                  color: customPrimaryGreyColor,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(10.0),
                                                child: Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    CircleAvatar(
                                                      backgroundColor:
                                                          Colors.white,
                                                      radius: 25,
                                                      child: ClipRRect(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(40),
                                                          child: getCanceledVideoRequestList[
                                                                          index]
                                                                      .user
                                                                      .profilePhotoUrl !=
                                                                  null
                                                              ? Image.network(
                                                                  '${getCanceledVideoRequestList[index].user.profilePhotoUrl}')
                                                              : Icon(
                                                                  Icons.person,
                                                                  size: 35,
                                                                  color: Colors
                                                                      .grey,
                                                                )),
                                                    ),
                                                    Expanded(
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .symmetric(
                                                                horizontal: 10),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: <Widget>[
                                                            Text(
                                                              '${getCanceledVideoRequestList[index].createdAt.split('T').first}',
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .headline4
                                                                  .copyWith(
                                                                    fontSize:
                                                                        12,
                                                                  ),
                                                              maxLines: 2,
                                                            ),
                                                            const SizedBox(
                                                              height: 8,
                                                            ),
                                                            Text(
                                                              '${getCanceledVideoRequestList[index].user.name}',
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .headline3
                                                                  .copyWith(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                  ),
                                                              softWrap: true,
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height: 30,
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        gradient:
                                                            LinearGradient(
                                                          colors: <Color>[
                                                            customGradientFirstColor,
                                                            customGradientSecondColor,
                                                          ],
                                                          end: Alignment
                                                              .bottomRight,
                                                        ),
                                                      ),
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .fromLTRB(
                                                                8, 0, 8, 0),
                                                        child: Center(
                                                            child: Text(
                                                          'Cancelados', //canceled
                                                          style: Theme.of(
                                                                  context)
                                                              .textTheme
                                                              .headline3
                                                              .copyWith(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize: 12),
                                                        )),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        );
                                      }),
                                    ),
                                    onRefresh: () async {
                                      getMethod(getVideoRequest, context,
                                          'getVideoRequests', null);
                                    }),

                        ///expire section
                        !appController.getVideoRequestCheck
                            ? ListView(
                                children: List<Widget>.generate(5, (int index) {
                                  return SkeletonLoader(
                                    highlightColor: Colors.grey,
                                    builder: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        height: 80,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                .95,
                                        decoration: BoxDecoration(
                                            color: customPrimaryGreyColor,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: Padding(
                                          padding: const EdgeInsets.all(10.0),
                                        ),
                                      ),
                                    ),
                                  );
                                }),
                              )
                            : getExpiredVideoRequestList.isEmpty
                                ? Center(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          'Nenhum pedido expirados!', //Your Expired orders are empty
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline1
                                              .copyWith(
                                                  fontWeight: FontWeight.w700),
                                        ),
                                      ],
                                    ),
                                  )
                                : RefreshIndicator(
                                    child: ListView(
                                      children: List<Widget>.generate(
                                          getExpiredVideoRequestList.length,
                                          (int index) {
                                        return InkWell(
                                          onTap: () {
                                            Get.find<AppController>()
                                                .updateGetSelectedVideoRequestModel(
                                                    updatedValue:
                                                        getExpiredVideoRequestList[
                                                            index]);
                                            Get.to(OrderDetailScreen());
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .95,
                                              decoration: BoxDecoration(
                                                  color: customPrimaryGreyColor,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(10.0),
                                                child: Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    CircleAvatar(
                                                      backgroundColor:
                                                          Colors.white,
                                                      radius: 25,
                                                      child: ClipRRect(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(40),
                                                          child: getExpiredVideoRequestList[
                                                                          index]
                                                                      .user
                                                                      .profilePhotoUrl !=
                                                                  null
                                                              ? Image.network(
                                                                  '${getExpiredVideoRequestList[index].user.profilePhotoUrl}')
                                                              : Icon(
                                                                  Icons.person,
                                                                  size: 35,
                                                                  color: Colors
                                                                      .grey,
                                                                )),
                                                    ),
                                                    Expanded(
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .symmetric(
                                                                horizontal: 10),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: <Widget>[
                                                            Text(
                                                              '${getExpiredVideoRequestList[index].createdAt.split('T').first}',
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .headline4
                                                                  .copyWith(
                                                                    fontSize:
                                                                        12,
                                                                  ),
                                                              maxLines: 2,
                                                            ),
                                                            const SizedBox(
                                                              height: 8,
                                                            ),
                                                            Text(
                                                              '${getExpiredVideoRequestList[index].user.name}',
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .headline3
                                                                  .copyWith(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                  ),
                                                              softWrap: true,
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height: 30,
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        gradient:
                                                            LinearGradient(
                                                          colors: <Color>[
                                                            customGradientFirstColor,
                                                            customGradientSecondColor,
                                                          ],
                                                          end: Alignment
                                                              .bottomRight,
                                                        ),
                                                      ),
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .fromLTRB(
                                                                8, 0, 8, 0),
                                                        child: Center(
                                                            child: Text(
                                                          'Expirados', //expired
                                                          style: Theme.of(
                                                                  context)
                                                              .textTheme
                                                              .headline3
                                                              .copyWith(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize: 12),
                                                        )),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        );
                                      }),
                                    ),
                                    onRefresh: () async {
                                      getMethod(getVideoRequest, context,
                                          'getVideoRequests', null);
                                    }),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
