import 'dart:async';
import 'dart:developer';
import 'package:fanfun_celebrity_app/controller/init_controller.dart';
import 'package:fanfun_celebrity_app/data/global_data.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:io' show Platform;

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  void navigator() {
    Navigator.of(context).pushReplacement(PageRouteBuilder<dynamic>(
        pageBuilder: (_, __, ___) => ScreenController(),
        transitionDuration: const Duration(milliseconds: 2000),
        transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
          return Opacity(
            opacity: animation.value,
            child: child,
          );
        }));
  }

  /// Set timer SplashScreenTemplate1
  Future<Timer> _timer() async {
    return Timer(const Duration(milliseconds: 4300), navigator);
  }

  AndroidNotificationChannel channel = const AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    'This channel is used for important notifications.', // description
    importance: Importance.high,
  );
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  @override
  void initState() {
    super.initState();
    _timer();

    if (!box.hasData('fcm_token')) {
      FirebaseMessaging.instance.getToken().then((dynamic value) {
        box.write('fcm_token', value);
        log('FirebaseMessagingToken------------->> $value');
        log('PlatForm------------->> ${Platform.operatingSystem}');
      });
    }

    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage message) {
      if (message != null) {}
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      log('FirebaseMessage------------->> $message');
      Get.snackbar(
          '${message.notification.title}', '${message.notification.body}',
          colorText: Colors.white);
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      log('A new onMessageOpenedApp event was published!');
    });
    FirebaseMessaging.onBackgroundMessage((RemoteMessage message) async {
      log('FirebaseMessagingBackground------------->> $message');
    });

    notification();
  }

  notification() async {
    FirebaseMessaging messaging = FirebaseMessaging.instance;

    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');
    } else if (settings.authorizationStatus ==
        AuthorizationStatus.provisional) {
      print('User granted provisional permission');
    } else {
      print('User declined or has not accepted permission');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration:
              BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/splashIcon.png'),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset('assets/splashBar.png'),
                  const SizedBox(
                    width: 8,
                  ),
                  Image.asset('assets/splashCelebrityIcon.png')
                ],
              )
            ],
          )),
    );
  }
}
