import 'package:fanfun_celebrity_app/data/global_data.dart';
import 'package:fanfun_celebrity_app/models/video_request_model.dart';
import 'package:fanfun_celebrity_app/screens/login_screen.dart';
import 'package:fanfun_celebrity_app/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UserProfileScreen extends StatefulWidget {
  @override
  _UserProfileScreenState createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends State<UserProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: InkWell(
          onTap: () {
            box.remove('accessToken');
            box.remove('isLogin');
            getVideoRequestList = <VideoRequestModel>[];
            getPendingVideoRequestList = <VideoRequestModel>[];
            getCompletedVideoRequestList = <VideoRequestModel>[];
            getCanceledVideoRequestList = <VideoRequestModel>[];
            getExpiredVideoRequestList = <VideoRequestModel>[];
            Get.offAll(LoginScreen());
          },
          child: Container(
            height: 50,
            width: MediaQuery.of(context).size.width * .45,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              gradient: const LinearGradient(
                colors: <Color>[
                  customGradientFirstColor,
                  customGradientSecondColor,
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              ),
            ),
            child: Center(
              child: Text(
                'Logout',
                style: Theme.of(context)
                    .textTheme
                    .headline3
                    .copyWith(fontSize: 18),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
