import 'package:fanfun_celebrity_app/components/gradient_text.dart';
import 'package:fanfun_celebrity_app/controller/app_controller.dart';
import 'package:fanfun_celebrity_app/data/global_decoration_data.dart';
import 'package:fanfun_celebrity_app/screens/forgot_password_screen.dart';
import 'package:fanfun_celebrity_app/services/api_urls.dart';
import 'package:fanfun_celebrity_app/services/post_services.dart';
import 'package:fanfun_celebrity_app/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> loginFormKey = GlobalKey();
  final TextEditingController loginEmailController = TextEditingController();
  final TextEditingController loginPasswordController = TextEditingController();

  bool obscureTextValue = true;

  // bool loader;
  // void loaderStateChange() {
  //   setState(() {
  //     loader = false;
  //   });
  // }
  //
  // @override
  // void initState() {
  //   super.initState();
  //   loader = false;
  // }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AppController>(
      init: AppController(),
      builder: (AppController appController) => ModalProgressHUD(
        progressIndicator: const CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.grey),
        ),
        inAsyncCall: appController.loaderProgressCheck,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            body: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: SizedBox(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: SafeArea(
                      child: Form(
                    key: loginFormKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .01,
                        ),
                        Image.asset(
                          'assets/splashIcon.png',
                          width: MediaQuery.of(context).size.width * .35,
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .03,
                        ),
                        Text(
                          'Login',
                          style: Theme.of(context)
                              .textTheme
                              .headline1
                              .copyWith(fontSize: 24),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .05,
                        ),

                        /// email field
                        Padding(
                            padding: const EdgeInsets.fromLTRB(24, 0, 0, 4),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Email',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline3
                                    .copyWith(color: customLightGreyColor),
                              ),
                            )),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(24, 0, 24, 16),
                          child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Theme.of(context).scaffoldBackgroundColor,
                            child: TextFormField(
                              style: const TextStyle(color: Colors.white),
                              controller: loginEmailController,
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                isDense: true,
                                filled: true,
                                fillColor: customLightGreyColor,
                                contentPadding: const EdgeInsets.symmetric(
                                    vertical: 15.0, horizontal: 10.0),
                                focusedBorder: customTextFieldFocusedBorder,
                                border: customTextFieldBorder,
                              ),
                              validator: (String value) {
                                const String pattern =
                                    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                final RegExp regex = RegExp(pattern);
                                if (value.isEmpty) {
                                  return 'Field Required';
                                } else if (!regex.hasMatch(value)) {
                                  return 'Please make sure'
                                      ' your email address is valid';
                                } else {
                                  return null;
                                }
                              },
                            ),
                          ),
                        ),

                        /// password field
                        Padding(
                            padding: const EdgeInsets.fromLTRB(24, 0, 0, 4),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Password',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline3
                                    .copyWith(color: customLightGreyColor),
                              ),
                            )),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(24, 0, 24, 24),
                          child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Theme.of(context).scaffoldBackgroundColor,
                            child: TextFormField(
                              style: const TextStyle(color: Colors.white),
                              controller: loginPasswordController,
                              obscureText: obscureTextValue,
                              decoration: InputDecoration(
                                  isDense: true,
                                  filled: true,
                                  fillColor: customLightGreyColor,
                                  contentPadding: const EdgeInsets.symmetric(
                                      vertical: 15.0, horizontal: 10.0),
                                  focusedBorder: customTextFieldFocusedBorder,
                                  border: customTextFieldBorder,
                                  suffixIcon: InkWell(
                                      onTap: () {
                                        setState(() {
                                          obscureTextValue = !obscureTextValue;
                                        });
                                      },
                                      child: Icon(
                                        obscureTextValue
                                            ? Icons.remove_red_eye
                                            : Icons.visibility_off,
                                        color: Colors.black,
                                      ))),
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Field Required';
                                }
                                return null;
                              },
                            ),
                          ),
                        ),

                        /// forgot password
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 24, 24),
                          child: Align(
                              alignment: Alignment.centerRight,
                              child: InkWell(
                                onTap: () {
                                  Get.to(ForgotPasswordScreen());
                                },
                                child: const GradientText(
                                  'Forgot Password?',
                                  fontSize: 16,
                                  gradient: LinearGradient(
                                    colors: <Color>[
                                      customGradientFirstColor,
                                      customGradientSecondColor,
                                    ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  ),
                                ),
                              )),
                        ),

                        /// login button
                        Padding(
                            padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                            child: InkWell(
                              onTap: () {
                                final FocusScopeNode currentFocus =
                                    FocusScope.of(context);
                                if (!currentFocus.hasPrimaryFocus) {
                                  currentFocus.unfocus();
                                }
                                if (loginFormKey.currentState.validate()) {
                                  Get.find<AppController>()
                                      .updateLoaderProgressCheck(
                                          updatedValue: true);
                                  postMethod(
                                      loginApi,
                                      <String, dynamic>{
                                        'email': loginEmailController.text,
                                        'password':
                                            loginPasswordController.text,
                                        'device_name': 'mobile'
                                      },
                                      'login',
                                      context,
                                      null);
                                }
                              },
                              child: Container(
                                height: 50,
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  gradient: const LinearGradient(
                                    colors: <Color>[
                                      customGradientFirstColor,
                                      customGradientSecondColor,
                                    ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Login',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(fontSize: 18),
                                  ),
                                ),
                              ),
                            )),
                        // SizedBox(
                        //   height: MediaQuery.of(context).size.height * .03,
                        // ),
                        //
                        // /// divider
                        // Padding(
                        //   padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                        //   child: Row(children: <Widget>[
                        //     const Expanded(
                        //       child: Divider(
                        //         color: customPrimaryGreyColor,
                        //       ),
                        //     ),
                        //     Padding(
                        //       padding: const EdgeInsets.fromLTRB(9, 0, 9, 0),
                        //       child: Text(
                        //         'or',
                        //         style: Theme.of(context)
                        //             .textTheme
                        //             .headline3
                        //             .copyWith(color: customPrimaryGreyColor),
                        //       ),
                        //     ),
                        //     const Expanded(
                        //       child: Divider(
                        //         color: customPrimaryGreyColor,
                        //       ),
                        //     ),
                        //   ]),
                        // ),
                        // SizedBox(
                        //   height: MediaQuery.of(context).size.height * .01,
                        // ),
                        //
                        // /// social buttons
                        // Padding(
                        //   padding: const EdgeInsets.fromLTRB(24, 0, 8, 0),
                        //   child: Row(
                        //     children: const <Widget>[
                        //       Expanded(
                        //           child: CustomGradientButton(
                        //         buttonHeight: 50,
                        //         textFontSize: 18,
                        //         text: 'Facebook',
                        //       )),
                        //       SizedBox(width: 20),
                        //       Expanded(
                        //           child: CustomGradientButton(
                        //         buttonHeight: 50,
                        //         textFontSize: 18,
                        //         text: 'Twitter',
                        //       )),
                        //     ],
                        //   ),
                        // ),
                        // SizedBox(
                        //   height: MediaQuery.of(context).size.height * .05,
                        // ),
                      ],
                    ),
                  )),
                ),
              ),
            )),
      ),
    );
  }
}
