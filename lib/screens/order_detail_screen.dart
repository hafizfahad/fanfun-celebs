import 'package:fanfun_celebrity_app/components/custom_gradient_button.dart';
import 'package:fanfun_celebrity_app/controller/app_controller.dart';
import 'package:fanfun_celebrity_app/data/global_decoration_data.dart';
import 'package:fanfun_celebrity_app/screens/telepromter_screen.dart';
import 'package:fanfun_celebrity_app/services/api_urls.dart';
import 'package:fanfun_celebrity_app/services/put_services.dart';
import 'package:fanfun_celebrity_app/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class OrderDetailScreen extends StatefulWidget {
  @override
  _OrderDetailScreenState createState() => _OrderDetailScreenState();
}

class _OrderDetailScreenState extends State<OrderDetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        leading: InkWell(
          onTap: () {
            Get.back();
          },
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: SvgPicture.asset(
              'assets/app_icons/arrows_icon/arrow_left.svg',
              color: Colors.white,
              fit: BoxFit.fill,
            ),
          ),
        ),
        elevation: 0,
        title: Text(
          /*Order Detail*/ 'Detalhes do Pedido',
          style: Theme.of(context).textTheme.headline1,
        ),
        centerTitle: true,
      ),
      body: GetBuilder<AppController>(
        init: AppController(),
        builder: (AppController appController) => ModalProgressHUD(
          progressIndicator: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.black),
          ),
          inAsyncCall: appController.loaderProgressCheck,
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Theme.of(context).scaffoldBackgroundColor,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Informações do cliente:', //user-info
                      style: Theme.of(context)
                          .textTheme
                          .headline1
                          .copyWith(color: customLightGreyColor),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 10),
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 25,
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(40),
                                  child: appController
                                              .getSelectedVideoRequestModel
                                              .user
                                              .profilePhotoUrl !=
                                          null
                                      ? Image.network(
                                          '${appController.getSelectedVideoRequestModel.user.profilePhotoUrl}')
                                      : Icon(
                                          Icons.person,
                                          size: 35,
                                          color: Colors.grey,
                                        )),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              '${appController.getSelectedVideoRequestModel.user.name}',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline1
                                  .copyWith(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Text(
                        'Informações do pedido:', //order-info
                        style: Theme.of(context)
                            .textTheme
                            .headline1
                            .copyWith(color: customLightGreyColor),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Status:',
                                  textAlign: TextAlign.end,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline3
                                      .copyWith(color: customLightGreyColor),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Text(
                                    appController.getSelectedVideoRequestModel
                                                .videoRequestStatus
                                                .toString() ==
                                            'waiting'
                                        ? 'Aguardando'
                                        : appController
                                                    .getSelectedVideoRequestModel
                                                    .videoRequestStatus
                                                    .toString() ==
                                                'canceled'
                                            ? 'Cancelados'
                                            : appController
                                                        .getSelectedVideoRequestModel
                                                        .videoRequestStatus
                                                        .toString() ==
                                                    'completed'
                                                ? 'Finalizados'
                                                : '',
                                    textAlign: TextAlign.start,
                                    style:
                                        Theme.of(context).textTheme.headline3,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    'Data:',
                                    textAlign: TextAlign.end,
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(color: customLightGreyColor),
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                      '${appController.getSelectedVideoRequestModel.createdAt.split('T').first}',
                                      textAlign: TextAlign.start,
                                      style:
                                          Theme.of(context).textTheme.headline3,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    'Para:', //to-whom
                                    textAlign: TextAlign.end,
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(color: customLightGreyColor),
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                      '${appController.getSelectedVideoRequestModel.toWhom}',
                                      textAlign: TextAlign.start,
                                      style:
                                          Theme.of(context).textTheme.headline3,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          appController.getSelectedVideoRequestModel.to == null
                              ? SizedBox()
                              : Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          'To:',
                                          textAlign: TextAlign.end,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3
                                              .copyWith(
                                                  color: customLightGreyColor),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(left: 10),
                                          child: Text(
                                            '${appController.getSelectedVideoRequestModel.to}',
                                            textAlign: TextAlign.start,
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline3,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    'Pronome:', //pronoun
                                    textAlign: TextAlign.end,
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(color: customLightGreyColor),
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                      '${appController.getSelectedVideoRequestModel.pronoun}',
                                      textAlign: TextAlign.start,
                                      style:
                                          Theme.of(context).textTheme.headline3,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    'Ocasião:', //occasion
                                    textAlign: TextAlign.end,
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(color: customLightGreyColor),
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                      '${appController.getSelectedVideoRequestModel.occasion}',
                                      textAlign: TextAlign.start,
                                      style:
                                          Theme.of(context).textTheme.headline3,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    'Instruções:', //instruction
                                    textAlign: TextAlign.end,
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(color: customLightGreyColor),
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                      '${appController.getSelectedVideoRequestModel.instructions}',
                                      textAlign: TextAlign.start,
                                      style:
                                          Theme.of(context).textTheme.headline3,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          appController.getSelectedVideoRequestModel
                                      .videoRequestStatus ==
                                  'completed'
                              ? Padding(
                                  padding: const EdgeInsets.only(top: 30),
                                  child: Container(
                                    height: 50,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: customLightGreyColor),
                                    child: Center(
                                      child: Text(
                                        'Finalizados', //completed
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline3
                                            .copyWith(fontSize: 18),
                                      ),
                                    ),
                                  ),
                                )
                              : appController.getSelectedVideoRequestModel
                                          .videoRequestStatus ==
                                      'canceled'
                                  ? Padding(
                                      padding: const EdgeInsets.only(top: 30),
                                      child: Container(
                                        height: 50,
                                        width: double.infinity,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            color: customLightGreyColor),
                                        child: Center(
                                          child: Text(
                                            'Cancelados', //canceled
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline3
                                                .copyWith(fontSize: 18),
                                          ),
                                        ),
                                      ),
                                    )
                                  : Column(
                                      children: <Widget>[
                                        InkWell(
                                          onTap: () {
                                            Get.to(TeleprompterScreen());
                                          },
                                          child: Padding(
                                            padding:
                                                const EdgeInsets.only(top: 30),
                                            child: Container(
                                              height: 50,
                                              width: double.infinity,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                gradient: const LinearGradient(
                                                  colors: <Color>[
                                                    customGradientFirstColor,
                                                    customGradientSecondColor,
                                                  ],
                                                  begin: Alignment.topLeft,
                                                  end: Alignment.bottomRight,
                                                ),
                                              ),
                                              child: Center(
                                                child: Text(
                                                  'Gravar Vídeo', //record
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline3
                                                      .copyWith(fontSize: 18),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            _showCancelDialog(context);
                                          },
                                          child: Padding(
                                            padding:
                                                const EdgeInsets.only(top: 15),
                                            child: CustomGradientButton(
                                              buttonHeight: 50,
                                              textFontSize: 18,
                                              text: 'Negar Pedido', //decline
                                            ),
                                          ),
                                        )
                                      ],
                                    )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  final TextEditingController cancelReasonController = TextEditingController();
  final GlobalKey<FormState> cancelFormKey = GlobalKey();
  Future<void> _showCancelDialog(BuildContext mainContext) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return GetBuilder<AppController>(
          init: AppController(),
          builder: (AppController appController) => AlertDialog(
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            title: Text(
              'Você tem certeza?', //are-u-sure
              style: Theme.of(mainContext).textTheme.headline3,
            ),
            content: SingleChildScrollView(
              child: Form(
                key: cancelFormKey,
                child: ListBody(
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 4),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Motivo', //reason
                            style: Theme.of(context)
                                .textTheme
                                .headline3
                                .copyWith(color: customLightGreyColor),
                          ),
                        )),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: Material(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Theme.of(context).scaffoldBackgroundColor,
                        child: TextFormField(
                          style: const TextStyle(color: Colors.white),
                          controller: cancelReasonController,
                          decoration: InputDecoration(
                            isDense: true,
                            filled: true,
                            fillColor: customLightGreyColor,
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 15.0, horizontal: 10.0),
                            focusedBorder: customTextFieldFocusedBorder,
                            border: customTextFieldBorder,
                          ),
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Field Required';
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        height: 35,
                        // width: 70,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: customLightGreyColor),
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: Text(
                              'Não cancelar', //cancel
                              style: Theme.of(context)
                                  .textTheme
                                  .headline3
                                  .copyWith(fontSize: 14),
                            ),
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        if (cancelFormKey.currentState.validate()) {
                          Navigator.pop(context);
                          Get.find<AppController>()
                              .updateLoaderProgressCheck(updatedValue: true);
                          putMethod(
                              '${getVideoRequest}/${appController.getSelectedVideoRequestModel.id}',
                              <String, dynamic>{
                                'cancelation_reason ':
                                    cancelReasonController.text,
                                'video_request_status': 'canceled'
                              },
                              'cancelRequest',
                              context,
                              null);
                        }
                      },
                      child: Container(
                        height: 35,
                        // width: 70,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          gradient: const LinearGradient(
                            colors: <Color>[
                              customGradientFirstColor,
                              customGradientSecondColor,
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                        ),
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: Text(
                              'Negar Pedido', //submit
                              style: Theme.of(context)
                                  .textTheme
                                  .headline3
                                  .copyWith(fontSize: 14),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
