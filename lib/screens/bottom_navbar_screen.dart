import 'package:fanfun_celebrity_app/screens/orders_screen.dart';
import 'package:fanfun_celebrity_app/screens/user_profile_screen.dart';
import 'package:fanfun_celebrity_app/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BottomNavBAr extends StatefulWidget {
  @override
  _BottomNavBArState createState() => _BottomNavBArState();
}

class _BottomNavBArState extends State<BottomNavBAr> {
  int currentIndex = 0;
  Widget callPage(int current) {
    switch (current) {
      case 0:
        return OrdersScreen();
        break;
      case 1:
        return UserProfileScreen();
        break;
      default:
        return OrdersScreen();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: callPage(currentIndex),
      bottomNavigationBar: Container(
        height: MediaQuery.of(context).size.height * .09,
        color: Theme.of(context).scaffoldBackgroundColor,
        child: ClipRRect(
            child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: customBottomBarColor,
          onTap: (int value) {
            setState(() {
              currentIndex = value;
            });
          },
          currentIndex: currentIndex,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                activeIcon: ShaderMask(
                  shaderCallback: (Rect bounds) {
                    return const RadialGradient(
                      center: Alignment.topLeft,
                      radius: 1,
                      colors: <Color>[
                        customGradientFirstColor,
                        customGradientSecondColor
                      ],
                      tileMode: TileMode.mirror,
                    ).createShader(bounds);
                  },
                  child: SvgPicture.asset(
                      'assets/app_icons/tab_bar_icon/orders.svg'),
                ),
                icon: SvgPicture.asset(
                  'assets/app_icons/tab_bar_icon/orders.svg',
                  color: customLightGreyColor,
                ),
                label: ''),
            BottomNavigationBarItem(
                activeIcon: ShaderMask(
                  shaderCallback: (Rect bounds) {
                    return const RadialGradient(
                      center: Alignment.topLeft,
                      radius: 1,
                      colors: <Color>[
                        customGradientFirstColor,
                        customGradientSecondColor
                      ],
                      tileMode: TileMode.mirror,
                    ).createShader(bounds);
                  },
                  child: SvgPicture.asset(
                    'assets/app_icons/tab_bar_icon/profile.svg',
                  ),
                ),
                icon: SvgPicture.asset(
                  'assets/app_icons/tab_bar_icon/profile.svg',
                  color: customLightGreyColor,
                ),
                label: '')
          ],
          showSelectedLabels: false,
          showUnselectedLabels: false,
        )),
      ),
    );
  }
}
