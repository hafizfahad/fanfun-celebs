import 'package:fanfun_celebrity_app/components/custom_gradient_button.dart';
import 'package:fanfun_celebrity_app/controller/app_controller.dart';
import 'package:fanfun_celebrity_app/screens/orders_screen.dart';
import 'package:fanfun_celebrity_app/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class OrderConfirmationScreen extends StatefulWidget {
  @override
  _OrderConfirmationScreenState createState() =>
      _OrderConfirmationScreenState();
}

class _OrderConfirmationScreenState extends State<OrderConfirmationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        leading: InkWell(
          onTap: () {
            Get.back();
          },
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: SvgPicture.asset(
              'assets/app_icons/arrows_icon/arrow_left.svg',
              color: Colors.white,
              fit: BoxFit.fill,
            ),
          ),
        ),
        elevation: 0,
        title: Text(
          /*Order Confirmation*/ 'Confirmação de pedido',
          style: Theme.of(context).textTheme.headline1,
        ),
        centerTitle: true,
      ),
      body: GetBuilder<AppController>(
        init: AppController(),
        builder: (AppController appController) => ModalProgressHUD(
          progressIndicator: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.black),
          ),
          inAsyncCall: appController.loaderProgressCheck,
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Theme.of(context).scaffoldBackgroundColor,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'User information:',
                      style: Theme.of(context)
                          .textTheme
                          .headline1
                          .copyWith(color: customLightGreyColor),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 10),
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 25,
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(40),
                                  child: appController
                                              .getSelectedVideoRequestModel
                                              .user
                                              .profilePhotoUrl !=
                                          null
                                      ? Image.network(
                                          '${appController.getSelectedVideoRequestModel.user.profilePhotoUrl}')
                                      : Icon(
                                          Icons.person,
                                          size: 35,
                                          color: Colors.grey,
                                        )),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              '${appController.getSelectedVideoRequestModel.user.name}',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline1
                                  .copyWith(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Text(
                        'Order information:',
                        style: Theme.of(context)
                            .textTheme
                            .headline1
                            .copyWith(color: customLightGreyColor),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Status:',
                                  textAlign: TextAlign.end,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline3
                                      .copyWith(color: customLightGreyColor),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Text(
                                    '${appController.getSelectedVideoRequestModel.videoRequestStatus}',
                                    textAlign: TextAlign.start,
                                    style:
                                        Theme.of(context).textTheme.headline3,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    'Date:',
                                    textAlign: TextAlign.end,
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(color: customLightGreyColor),
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                      '${appController.getSelectedVideoRequestModel.createdAt.split('T').first}',
                                      textAlign: TextAlign.start,
                                      style:
                                          Theme.of(context).textTheme.headline3,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    'To Whom:',
                                    textAlign: TextAlign.end,
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(color: customLightGreyColor),
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                      '${appController.getSelectedVideoRequestModel.toWhom}',
                                      textAlign: TextAlign.start,
                                      style:
                                          Theme.of(context).textTheme.headline3,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          appController.getSelectedVideoRequestModel.to == null
                              ? SizedBox()
                              : Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          'To:',
                                          textAlign: TextAlign.end,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3
                                              .copyWith(
                                                  color: customLightGreyColor),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(left: 10),
                                          child: Text(
                                            '${appController.getSelectedVideoRequestModel.to}',
                                            textAlign: TextAlign.start,
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline3,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    'Pronoun:',
                                    textAlign: TextAlign.end,
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(color: customLightGreyColor),
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                      '${appController.getSelectedVideoRequestModel.pronoun}',
                                      textAlign: TextAlign.start,
                                      style:
                                          Theme.of(context).textTheme.headline3,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    'Occasion:',
                                    textAlign: TextAlign.end,
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(color: customLightGreyColor),
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                      '${appController.getSelectedVideoRequestModel.occasion}',
                                      textAlign: TextAlign.start,
                                      style:
                                          Theme.of(context).textTheme.headline3,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    'Instructions:',
                                    textAlign: TextAlign.end,
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(color: customLightGreyColor),
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                      '${appController.getSelectedVideoRequestModel.instructions}',
                                      textAlign: TextAlign.start,
                                      style:
                                          Theme.of(context).textTheme.headline3,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Column(
                            children: <Widget>[
                              InkWell(
                                onTap: () {},
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 30),
                                  child: Container(
                                    height: 50,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      gradient: const LinearGradient(
                                        colors: <Color>[
                                          customGradientFirstColor,
                                          customGradientSecondColor,
                                        ],
                                        begin: Alignment.topLeft,
                                        end: Alignment.bottomRight,
                                      ),
                                    ),
                                    child: Center(
                                      child: Text(
                                        'Send',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline3
                                            .copyWith(fontSize: 18),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  Get.offAll(OrdersScreen());
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 15),
                                  child: CustomGradientButton(
                                    buttonHeight: 50,
                                    textFontSize: 18,
                                    text: 'Discard',
                                  ),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
