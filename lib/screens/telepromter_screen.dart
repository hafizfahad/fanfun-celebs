import 'dart:async';
import 'dart:developer';
import 'dart:io';
import 'package:camera/camera.dart';
import 'package:chewie/chewie.dart';
import 'package:dio/dio.dart' as dio_instance;
import 'package:fanfun_celebrity_app/controller/app_controller.dart';
import 'package:fanfun_celebrity_app/data/global_data.dart';
import 'package:fanfun_celebrity_app/screens/bottom_navbar_screen.dart';
import 'package:fanfun_celebrity_app/services/api_urls.dart';
import 'package:fanfun_celebrity_app/services/header_services.dart';
import 'package:fanfun_celebrity_app/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:video_player/video_player.dart';
import 'package:http_parser/http_parser.dart';
import 'package:get/route_manager.dart';

class TeleprompterScreen extends StatefulWidget {
  @override
  _TeleprompterScreenState createState() {
    return _TeleprompterScreenState();
  }
}

void logError(String code, String message) =>
    log('Error: $code\nError Message: $message');

class _TeleprompterScreenState extends State<TeleprompterScreen>
    with WidgetsBindingObserver, TickerProviderStateMixin {
  uploadImage(XFile file) async {
    String fileName = file.path.split('/').last;
    dio_instance.FormData formData =
        dio_instance.FormData.fromMap(<String, dynamic>{
      '_method': 'put',
      'video_request_status': 'completed',
      'video': await dio_instance.MultipartFile.fromFile(
        file.path,
        filename: fileName,
        contentType: new MediaType('video', 'mp4'), //important
      ),
    });
    dio_instance.Dio dio = dio_instance.Dio();
    setHeader(dio, 'Authorization', 'Bearer ${box.read('accessToken')}');
    dio_instance.Response<dynamic> response;
    try {
      response = await dio.post(
          '${getVideoRequest}/'
          '${Get.find<AppController>().getSelectedVideoRequestModel.id}',
          data: formData);
      Get.find<AppController>().updateLoaderProgressCheck(updatedValue: false);
      log('putStatusCode---->> ${response.statusCode}');
      log('putResponse---->> ${response.data}');
      if (response.statusCode.toString() == '200') {
        setState(() {});
      }
      Get.offAll(BottomNavBAr());
    } on dio_instance.DioError catch (e) {
      Get.find<AppController>().updateLoaderProgressCheck(updatedValue: false);
      // Get.offAll(BottomNavBAr());
      log('putResponseError---->> ${e}');
    }
  }

  CameraController controller;
  XFile videoFile;
  bool enableAudio = true;
  double _minAvailableZoom;
  double _maxAvailableZoom;
  num _currentScale = 1.0;
  double _baseScale = 1.0;
  int _pointers = 0;
  CameraDescription cameraDescriptionTest = cameras.firstWhere(
      (CameraDescription description) =>
          description.lensDirection == CameraLensDirection.front);

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    onNewCameraSelected(cameraDescriptionTest);
    initPlatformState();
  }

  Future<void> initPlatformState() async {
    // await Flurry.initialize(
    //     androidKey: "YZR4NX8BSBJT3B3M33W7",
    //     // iosKey: "XCCWQH4MCD45JHSM4BYN",
    //
    //     enableLog: true);
    // await Flurry.setUserId("1234");
    // await Flurry.logEvent("TelePromoter Screen");
  }

  void _toggleCameraLens() {
    // get current lens direction (front / rear)
    final dynamic lensDirection = controller.description.lensDirection;
    CameraDescription newDescription;
    if (lensDirection == CameraLensDirection.front) {
      newDescription = cameras.firstWhere((CameraDescription description) =>
          description.lensDirection == CameraLensDirection.back);
    } else {
      newDescription = cameras.firstWhere((CameraDescription description) =>
          description.lensDirection == CameraLensDirection.front);
    }
    if (newDescription != null) {
      onNewCameraSelected(newDescription);
    } else {
      log('Asked camera not available');
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // App state changed before we got the chance to initialize.
    if (controller == null || !controller.value.isInitialized) {
      return;
    }
    if (state == AppLifecycleState.inactive) {
      controller?.dispose();
    } else if (state == AppLifecycleState.resumed) {
      if (controller != null) {
        onNewCameraSelected(controller.description);
      }
    }
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  void rebuildAllChildren(BuildContext context) {
    void rebuild(Element el) {
      el.markNeedsBuild();
      el.visitChildren(rebuild);
    }

    (context as Element).visitChildren(rebuild);
  }

  @override
  Widget build(BuildContext context) {
    rebuildAllChildren(context);
    final Size size = MediaQuery.of(context).size;
    final CameraValue camera = controller.value;

    // calculate scale depending on screen and camera ratios
    // this is actually size.aspectRatio / (1 / camera.aspectRatio)
    // because camera preview size is received as landscape
    // but we're calculating for portrait orientation
    double cameraScale = size.aspectRatio * camera.aspectRatio;

    // to prevent scaling down, invert the value
    if (cameraScale < 1) cameraScale = 1 / cameraScale;
    return Scaffold(
      key: _scaffoldKey,
      body: GetBuilder<AppController>(
        init: AppController(),
        builder: (AppController appController) => ModalProgressHUD(
          progressIndicator: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.black),
          ),
          inAsyncCall: appController.loaderProgressCheck,
          child: videoFile != null
              ? Stack(
                  children: <Widget>[
                    SizedBox(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: Transform.scale(
                        scale: cameraScale,
                        child: Chewie(
                          controller: chewieVideoController,
                        ),
                      ),
                    ),
                    Positioned(
                      top: 10,
                      child: SizedBox(
                        height: 70,
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            ///-----video-discard-button
                            InkWell(
                              onTap: () {
                                Get.back();
                              },
                              child: Container(
                                height: 40,
                                width:
                                    MediaQuery.of(context).size.width * .35,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: customLightGreyColor),
                                child: Center(
                                  child: Text(
                                    'Discard',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(fontSize: 18),
                                  ),
                                ),
                              ),
                            ),

                            ///-----video-send-button
                            InkWell(
                              onTap: () {
                                Get.find<AppController>()
                                    .updateLoaderProgressCheck(
                                        updatedValue: true);
                                uploadImage(videoFile);
                              },
                              child: Container(
                                height: 40,
                                width:
                                    MediaQuery.of(context).size.width * .35,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  gradient: const LinearGradient(
                                    colors: <Color>[
                                      customGradientFirstColor,
                                      customGradientSecondColor,
                                    ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Send',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(fontSize: 18),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )
              : Stack(
                  children: <Widget>[
                    SizedBox(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: Transform.scale(
                        scale: cameraScale,
                        child: Center(child: _cameraPreviewWidget()),
                      ),
                    ),

                    ///----camera-rotator
                    Positioned(
                        top: 5,
                        child: SizedBox(
                          height: 50,
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(right: 30),
                                child: InkWell(
                                  onTap: () {
                                    _toggleCameraLens();
                                  },
                                  child: const Icon(Icons.rotate_right,
                                      size: 35,
                                      color: customGradientSecondColor),
                                ),
                              )
                            ],
                          ),
                        )),

                    ///----message-show-box
                    Positioned(
                        bottom: 90,
                        child: GetBuilder<AppController>(
                          init: AppController(),
                          builder: (AppController appController) => Container(
                            // height: MediaQuery.of(context).size.height*.,
                            width: MediaQuery.of(context).size.width,
                            color: Colors.transparent,
                            child: Column(
                              children: <Widget>[
                                Center(
                                  child: Container(
                                    // height: MediaQuery.of(context).size.height*.,
                                    width: MediaQuery.of(context).size.width *
                                        .8,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(10),
                                        color: Colors.white),
                                    child: Padding(
                                      padding: EdgeInsets.all(10.0),
                                      child: Text(
                                          '${appController.getSelectedVideoRequestModel.instructions}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3
                                              .copyWith(
                                                  color:
                                                      customPrimaryGreyColor)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )),

                    ///----video-controller
                    Positioned(
                      bottom: 5,
                      left: 40,
                      child: Container(
                        height: MediaQuery.of(context).size.height * .1,
                        width: MediaQuery.of(context).size.width * .9,
                        color: Colors.transparent,
                        child: (controller != null &&
                                controller.value.isInitialized &&
                                controller.value.isRecordingVideo)
                            ? Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  ///----finish-Controller
                                  InkWell(
                                      onTap: controller != null &&
                                              controller
                                                  .value.isInitialized &&
                                              controller
                                                  .value.isRecordingVideo
                                          ? onStopButtonPressed
                                          : null,
                                      child: Container(
                                        height: 50,
                                        width: 200,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          gradient: const LinearGradient(
                                            colors: <Color>[
                                              customGradientFirstColor,
                                              customGradientSecondColor,
                                            ],
                                            begin: Alignment.topLeft,
                                            end: Alignment.bottomRight,
                                          ),
                                        ),
                                        child: Center(
                                          child: Text(
                                            'Stop',
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline3
                                                .copyWith(fontSize: 18),
                                          ),
                                        ),
                                      )),
                                ],
                              )

                            ///----start-Controller
                            : Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  ///-----back-button
                                  InkWell(
                                    onTap: () {
                                      Get.back();
                                    },
                                    child: Container(
                                        height: 48,
                                        width: 48,
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color:
                                                    customGradientSecondColor),
                                            borderRadius:
                                                BorderRadius.circular(40),
                                            color: Colors.transparent),
                                        child: Center(
                                          child: SvgPicture.asset(
                                            'assets/app_icons/arrows_icon/arrow_left.svg',
                                            color: customGradientFirstColor,
                                          ),
                                        )),
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),

                                  ///-----record-button
                                  InkWell(
                                    onTap: controller != null &&
                                            controller.value.isInitialized &&
                                            !controller.value.isRecordingVideo
                                        ? onVideoRecordButtonPressed
                                        : null,
                                    child: Container(
                                      height: 50,
                                      width: 200,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(10),
                                        gradient: const LinearGradient(
                                          colors: <Color>[
                                            customGradientFirstColor,
                                            customGradientSecondColor,
                                          ],
                                          begin: Alignment.topLeft,
                                          end: Alignment.bottomRight,
                                        ),
                                      ),
                                      child: Center(
                                        child: Text(
                                          'Tap to record',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3
                                              .copyWith(fontSize: 16),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                      ),
                    ),
                  ],
                ),
        ),
      ),
    );
  }

  /// Display the preview from the camera
  /// (or a message if the preview is not available).
  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return const Text(
        'Tap a camera',
        style: TextStyle(
          color: Colors.white,
          fontSize: 24.0,
          fontWeight: FontWeight.w900,
        ),
      );
    } else {
      return Listener(
        onPointerDown: (_) => _pointers++,
        onPointerUp: (_) => _pointers--,
        child: CameraPreview(
          controller,
          child: LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constraints) {
            return GestureDetector(
              behavior: HitTestBehavior.opaque,
              onScaleStart: _handleScaleStart,
              onScaleUpdate: _handleScaleUpdate,
              onTapDown: (TapDownDetails details) =>
                  onViewFinderTap(details, constraints),
            );
          }),
        ),
      );
    }
  }

  void _handleScaleStart(ScaleStartDetails details) {
    _baseScale = _currentScale.toDouble();
  }

  Future<void> _handleScaleUpdate(ScaleUpdateDetails details) async {
    // When there are not exactly two fingers on screen don't scale
    if (_pointers != 2) {
      return;
    }

    _currentScale = (_baseScale * details.scale)
        .clamp(_minAvailableZoom, _maxAvailableZoom);

    await controller.setZoomLevel(_currentScale.toDouble());
  }

  void showInSnackBar(String message) {
    // ignore: deprecated_member_use
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message)));
  }

  void onViewFinderTap(TapDownDetails details, BoxConstraints constraints) {
    final Offset offset = Offset(
      details.localPosition.dx / constraints.maxWidth,
      details.localPosition.dy / constraints.maxHeight,
    );
    controller.setExposurePoint(offset);
    controller.setFocusPoint(offset);
  }

  Future<void> onNewCameraSelected(CameraDescription cameraDescription) async {
    if (controller != null) {
      log('--controllerNotNull');
      await controller.dispose();
    }
    controller = CameraController(
      cameraDescription,
      ResolutionPreset.medium,
      enableAudio: enableAudio,
      imageFormatGroup: ImageFormatGroup.jpeg,
    );

    // If the controller is updated then update the UI.
    controller.addListener(() {
      if (mounted) setState(() {});
      if (controller.value.hasError) {
        showInSnackBar('Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
      log('1 ${controller.value.isInitialized}');
    } on CameraException catch (e) {
      _showCameraException(e);
    }

    if (mounted) {
      setState(() {});
    }
  }

  void onVideoRecordButtonPressed() {
    startVideoRecording().then((_) {
      if (mounted) setState(() {});
    });
  }

  ChewieController chewieVideoController;
  void onStopButtonPressed() {
    stopVideoRecording().then((XFile file) {
      if (mounted) setState(() {});
      if (file != null) {
        setState(() {
          videoFile = file;
        });

        chewieVideoController = ChewieController(
          deviceOrientationsOnEnterFullScreen: <DeviceOrientation>[
            DeviceOrientation.portraitUp
          ],
          deviceOrientationsAfterFullScreen: <DeviceOrientation>[
            DeviceOrientation.portraitUp
          ],
          videoPlayerController:
              VideoPlayerController.file(File(videoFile.path))..initialize(),
          fullScreenByDefault: false,
          showControls: true,
          aspectRatio: 1 / 1.4,
        );
        // _startVideoPlayer();
      }
    });
  }

  void onPauseButtonPressed() {
    pauseVideoRecording().then((_) {
      if (mounted) setState(() {});
      showInSnackBar('Video recording paused');
    });
  }

  void onResumeButtonPressed() {
    resumeVideoRecording().then((_) {
      if (mounted) setState(() {});
      showInSnackBar('Video recording resumed');
    });
  }

  Future<void> startVideoRecording() async {
    if (!controller.value.isInitialized) {
      showInSnackBar('Error: select a camera first.');
      return;
    }

    if (controller.value.isRecordingVideo) {
      // A recording is already started, do nothing.
      return;
    }
    log('3---${controller.value}');
    try {
      await controller.startVideoRecording();
      log('2');
    } on CameraException catch (e) {
      log('2----error');
      _showCameraException(e);
      return;
    }
  }

  Future<XFile> stopVideoRecording() async {
    if (!controller.value.isRecordingVideo) {
      return null;
    }

    try {
      return controller.stopVideoRecording();
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
  }

  Future<void> pauseVideoRecording() async {
    if (!controller.value.isRecordingVideo) {}

    try {
      await controller.pauseVideoRecording();
    } on CameraException catch (e) {
      _showCameraException(e);
      rethrow;
    }
  }

  Future<void> resumeVideoRecording() async {
    if (!controller.value.isRecordingVideo) {}

    log('controller.value.isRecordingVideo');
    try {
      await controller.resumeVideoRecording();
    } on CameraException catch (e) {
      _showCameraException(e);
      rethrow;
    }
  }

  void _showCameraException(CameraException e) {
    logError(e.code, e.description);
    showInSnackBar('Error: ${e.code}\n${e.description}');
  }
}

List<CameraDescription> cameras = <CameraDescription>[];
