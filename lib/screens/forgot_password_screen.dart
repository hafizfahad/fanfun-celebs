import 'package:fanfun_celebrity_app/components/gradient_text.dart';
import 'package:fanfun_celebrity_app/data/global_decoration_data.dart';
import 'package:fanfun_celebrity_app/screens/login_screen.dart';
import 'package:fanfun_celebrity_app/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final GlobalKey<FormState> forgotFormKey = GlobalKey<FormState>();

  final TextEditingController forgotEmailController = TextEditingController();

  bool loader;

  void checkChange() {
    setState(() {
      loader = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loader = false;
  }

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      progressIndicator: const CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.grey),
      ),
      inAsyncCall: loader,
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: SizedBox(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: SafeArea(
                      child: Form(
                    key: forgotFormKey,
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .15,
                        ),
                        Image.asset(
                          'assets/splashIcon.png',
                          width: MediaQuery.of(context).size.width * .35,
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .05,
                        ),
                        Text(
                          'Reset Password',
                          style: Theme.of(context)
                              .textTheme
                              .headline1
                              .copyWith(fontSize: 24),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .05,
                        ),

                        /// email field
                        Padding(
                            padding: const EdgeInsets.fromLTRB(24, 0, 0, 4),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Email',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline3
                                    .copyWith(color: customLightGreyColor),
                              ),
                            )),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                          child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Theme.of(context).scaffoldBackgroundColor,
                            child: TextFormField(
                              style: const TextStyle(color: Colors.white),
                              controller: forgotEmailController,
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                isDense: true,
                                filled: true,
                                fillColor: customLightGreyColor,
                                contentPadding: const EdgeInsets.symmetric(
                                    vertical: 15.0, horizontal: 10.0),
                                focusedBorder: customTextFieldFocusedBorder,
                                border: customTextFieldBorder,
                              ),
                              validator: (String value) {
                                const String pattern =
                                    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                final RegExp regex = RegExp(pattern);
                                if (value.isEmpty) {
                                  return 'Field Required';
                                } else if (!regex.hasMatch(value)) {
                                  return 'Please make sure your'
                                      ' email address is valid';
                                } else {
                                  return null;
                                }
                              },
                            ),
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .05,
                        ),

                        /// reset button
                        Padding(
                            padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                            child: InkWell(
                              onTap: () {
                                if (forgotFormKey.currentState.validate()) {}
                              },
                              child: Container(
                                height: 50,
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  gradient: const LinearGradient(
                                    colors: <Color>[
                                      customGradientFirstColor,
                                      customGradientSecondColor,
                                    ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Reset',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(fontSize: 18),
                                  ),
                                ),
                              ),
                            )),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .1,
                        ),

                        /// login option
                        Text(
                          'Remember your password?',
                          style: Theme.of(context).textTheme.headline3,
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .02,
                        ),
                        InkWell(
                          onTap: () {
                            Get.offAll(LoginScreen());
                          },
                          child: const GradientText(
                            'Login',
                            fontSize: 16,
                            gradient: LinearGradient(
                              colors: <Color>[
                                customGradientFirstColor,
                                customGradientSecondColor,
                              ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                            ),
                          ),
                        )
                      ],
                    ),
                  ))),
            ),
          )),
    );
  }
}
